package Entity;

public class Voucher {
    private String id;
    private String voucher_type;
    private String transport;
    private String country;
    private String number_days;
    private String number_nights;
    private String hotel_owner;
    private String hotel_name;
    private String stars;
    private String board_type;
    private String room_bed_amount;
    private String available_amenities;
    private double cost;
    private String included_factors_in_price;

    public void setId(String id) {
        this.id = id;
    }

    public void setVoucher_type(String voucher_type) {
        this.voucher_type = voucher_type;
    }

    public void setTransport(String transport) {
        this.transport = transport;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setNumber_days(String number_days) {
        this.number_days = number_days;
    }

    public void setNumber_nights(String number_nights) {
        this.number_nights = number_nights;
    }

    public void setHotel_owner(String hotel_owner) {
        this.hotel_owner = hotel_owner;
    }

    public void setHotel_name(String hotel_name) {
        this.hotel_name = hotel_name;
    }

    public void setStars(String stars) {
        this.stars = stars;
    }

    public void setBoard_type(String board_type) {
        this.board_type = board_type;
    }

    public void setRoom_bed_amount(String room_bed_amount) {
        this.room_bed_amount = room_bed_amount;
    }

    public void setAvailable_amenities(String available_amenities) {
        this.available_amenities = available_amenities;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public void setIncluded_factors_in_price(String included_factors_in_price) {
        this.included_factors_in_price = included_factors_in_price;
    }

    public String getId() {
        return id;
    }

    public String getVoucher_type() {
        return voucher_type;
    }

    public String getTransport() {
        return transport;
    }

    public String getCountry() {
        return country;
    }

    public String getNumber_days() {
        return number_days;
    }

    public String getNumber_nights() {
        return number_nights;
    }

    public String getHotel_owner() {
        return hotel_owner;
    }

    public String getHotel_name() {
        return hotel_name;
    }

    public String getStars() {
        return stars;
    }

    public String getBoard_type() {
        return board_type;
    }

    public String getRoom_bed_amount() {
        return room_bed_amount;
    }

    public String getAvailable_amenities() {
        return available_amenities;
    }

    public double getCost() {
        return cost;
    }

    public String getIncluded_factors_in_price() {
        return included_factors_in_price;
    }
}
