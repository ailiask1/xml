package Entity;

public enum VoucherTagName {
    VOUCHERS, VOUCHER, COUNTRY, NUMBER_DAYS, NUMBER_NIGHTS, HOTEL_CHARACTERISTICS, HOTEL_NAME, STARS,
    BOARD_TYPE, ROOM_BED_AMOUNT, AVAILABLE_AMENITIES, COST, INCLUDED_FACTORS_IN_PRICE;

    public static VoucherTagName getElementTagName(String element) {
        switch (element) {
            case "Vouchers":
                return VOUCHERS;
            case "Voucher":
                return VOUCHER;
            case "Country":
                return COUNTRY;
            case "Number_days":
                return NUMBER_DAYS;
            case "Number_nights":
                return NUMBER_NIGHTS;
            case "Hotel_characteristics":
                return HOTEL_CHARACTERISTICS;
            case "Hotel_name":
                return HOTEL_NAME;
            case "Stars":
                return STARS;
            case "Board_type":
                return BOARD_TYPE;
            case "Room_bed_amount":
                return ROOM_BED_AMOUNT;
            case "Available_amenities":
                return AVAILABLE_AMENITIES;
            case "Cost":
                return COST;
            case "Included_factors_in_price":
                return INCLUDED_FACTORS_IN_PRICE;
            default:
                throw new EnumConstantNotPresentException(VoucherTagName.class,
                        element);
        }
    }
}
