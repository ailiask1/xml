package Parser;

import Entity.Voucher;
import com.sun.org.apache.xerces.internal.parsers.DOMParser;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.stream.*;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class DomParser {

    public static void run() throws IOException, SAXException, XMLStreamException {
        DOMParser parser = new DOMParser();
        parser.parse("src\\main\\resources\\Tourist_vouchers.xml");
        Document document = parser.getDocument();
        Element root = document.getDocumentElement();
        List<Voucher> voucherList = new ArrayList<Voucher>();
        NodeList voucherNodes = root.getElementsByTagName("Voucher");
        Voucher voucher = null;
        for (int i = 0; i < voucherNodes.getLength(); i++) {
            voucher = new Voucher();
            Element voucherElement = (Element) voucherNodes.item(i);
            voucher.setId(voucherElement.getAttribute("id"));
            voucher.setVoucher_type(voucherElement.getAttribute("Voucher_type"));
            voucher.setTransport(voucherElement.getAttribute("Transport"));
            voucher.setCountry(getSingleChild(voucherElement, "Country").getTextContent().trim());
            voucher.setNumber_days(getSingleChild(voucherElement, "Number_days").getTextContent().trim());
            voucher.setNumber_nights(getSingleChild(voucherElement, "Number_nights").getTextContent().trim());
            voucher.setIncluded_factors_in_price(getSingleChild(voucherElement, "Included_factors_in_price").getTextContent().trim());
            voucher.setCost(Double.parseDouble(getSingleChild(voucherElement, "Cost").getTextContent().trim()));
            Element hotel = getSingleChild(voucherElement, "Hotel_characteristics");
            String hotel_owner = hotel.getAttribute("Hotel_owner");
            if (hotel_owner != null) {
                voucher.setHotel_owner(hotel_owner);
            } else {
                voucher.setHotel_owner("Unknown");
            }
            voucher.setHotel_name(getSingleChild(hotel, "Hotel_name").getTextContent().trim());
            voucher.setStars(getSingleChild(hotel, "Stars").getTextContent().trim());
            voucher.setBoard_type(getSingleChild(hotel, "Board_type").getTextContent().trim());
            voucher.setRoom_bed_amount(getSingleChild(hotel, "Room_bed_amount").getTextContent().trim());
            voucher.setAvailable_amenities(getSingleChild(hotel, "Available_amenities").getTextContent().trim());
            voucherList.add(voucher);
        }

        XMLOutputFactory factory = XMLOutputFactory.newInstance();
        XMLStreamWriter writer = factory.createXMLStreamWriter(
                new FileWriter("DomParser.xml"));
        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        try {
            InputStream input = new FileInputStream("src\\main\\resources\\Tourist_vouchers.xml");
            XMLStreamReader reader = inputFactory.createXMLStreamReader(input);
            writer.writeStartDocument();
            writer.writeStartElement("document");
            for (Voucher element : voucherList) {
                writer.writeStartElement("Voucher");
                writer.writeAttribute("id", element.getId());
                writer.writeAttribute("Voucher_type", element.getVoucher_type());
                writer.writeAttribute("Transport", element.getTransport());
                writer.writeStartElement("Country");
                writer.writeCharacters(element.getCountry());
                writer.writeEndElement();
                writer.writeStartElement("Number_days");
                writer.writeCharacters(element.getNumber_days());
                writer.writeEndElement();
                writer.writeStartElement("Number_nights");
                writer.writeCharacters(element.getNumber_nights());
                writer.writeEndElement();
                writer.writeStartElement("Cost");
                writer.writeCharacters(Double.toString(element.getCost()));
                writer.writeEndElement();
                writer.writeStartElement("Hotel_characteristics");
                writer.writeAttribute("Hotel_owner", element.getHotel_owner());
                writer.writeStartElement("Hotel_name");
                writer.writeCharacters(element.getHotel_name());
                writer.writeEndElement();
                writer.writeStartElement("Stars");
                writer.writeCharacters(element.getStars());
                writer.writeEndElement();
                writer.writeStartElement("Board_type");
                writer.writeCharacters(element.getBoard_type());
                writer.writeEndElement();
                writer.writeEndElement();
                writer.writeEndElement();
            }
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
        writer.writeEndElement();
        writer.writeEndDocument();
        writer.flush();
        writer.close();
    }

    private static Element getSingleChild(Element element, String childName) {
        NodeList nlist = element.getElementsByTagName(childName);
        Element child = (Element) nlist.item(0);
        return child;
    }

}
