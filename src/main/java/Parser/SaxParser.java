package Parser;

import Filter.ElementFilter;
import Filter.NamespaceFilter;
import Handler.MenuSaxHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.IOException;

public class SaxParser extends DefaultHandler {

    public static void run() throws IOException, SAXException {
        XMLReader reader = XMLReaderFactory.createXMLReader();
        NamespaceFilter namespaceFilter = new NamespaceFilter(reader);
        ElementFilter elementFilter = new ElementFilter();
        elementFilter.setParent(namespaceFilter);
        MenuSaxHandler handler = new MenuSaxHandler();
        elementFilter.setContentHandler(handler);
        elementFilter.parse(new InputSource("src\\main\\resources\\Tourist_vouchers.xml"));
    }

}
