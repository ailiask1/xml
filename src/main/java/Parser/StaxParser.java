package Parser;

import Entity.Voucher;
import Entity.VoucherTagName;

import javax.xml.stream.*;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class StaxParser {

    public static void run() throws IOException, XMLStreamException {

        XMLOutputFactory factory = XMLOutputFactory.newInstance();
        XMLStreamWriter writer = factory.createXMLStreamWriter(
                new FileWriter("StaxParser.xml"));
        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        try {
            InputStream input = new FileInputStream("src\\main\\resources\\Tourist_vouchers.xml");
            XMLStreamReader reader = inputFactory.createXMLStreamReader(input);
            List<Voucher> voucherList = process(reader);
            writer.writeStartDocument();
            writer.writeStartElement("document");
            for (Voucher voucher : voucherList) {
                writer.writeStartElement("Voucher");
                writer.writeAttribute("id", voucher.getId());
                writer.writeAttribute("Voucher_type", voucher.getVoucher_type());
                writer.writeAttribute("Transport", voucher.getTransport());
                writer.writeStartElement("Country");
                writer.writeCharacters(voucher.getCountry());
                writer.writeEndElement();
                writer.writeStartElement("Number_days");
                writer.writeCharacters(voucher.getNumber_days());
                writer.writeEndElement();
                writer.writeStartElement("Number_nights");
                writer.writeCharacters(voucher.getNumber_nights());
                writer.writeEndElement();
                writer.writeStartElement("Cost");
                writer.writeCharacters(Double.toString(voucher.getCost()));
                writer.writeEndElement();
                writer.writeStartElement("Hotel_characteristics");
                writer.writeAttribute("Hotel_owner", voucher.getHotel_owner());
                writer.writeStartElement("Hotel_name");
                writer.writeCharacters(voucher.getHotel_name());
                writer.writeEndElement();
                writer.writeStartElement("Stars");
                writer.writeCharacters(voucher.getStars());
                writer.writeEndElement();
                writer.writeStartElement("Board_type");
                writer.writeCharacters(voucher.getBoard_type());
                writer.writeEndElement();
                writer.writeEndElement();
                writer.writeEndElement();
            }
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
        writer.writeEndElement();
        writer.writeEndDocument();
        writer.flush();
        writer.close();

    }

    private static List<Voucher> process(XMLStreamReader reader) throws XMLStreamException {
        List<Voucher> voucherList = new ArrayList<Voucher>();
        Voucher voucher = null;
        VoucherTagName elementName = null;
        while (reader.hasNext()) {
            int type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    elementName = VoucherTagName.getElementTagName(reader.getLocalName());
                    switch (elementName) {
                        case VOUCHER:
                            voucher = new Voucher();
                            voucher.setId(reader.getAttributeValue(null, "id"));
                            voucher.setVoucher_type(reader.getAttributeValue(null, "Voucher_type"));
                            voucher.setTransport(reader.getAttributeValue(null, "Transport"));
                            break;
                        case HOTEL_CHARACTERISTICS:
                            String attribute = reader.getAttributeValue(null, "Hotel_owner");
                            if (attribute != null) {
                                voucher.setHotel_owner(attribute);
                            } else {
                                voucher.setHotel_owner("Uknown");
                            }
                            break;
                    }
                    break;
                case XMLStreamConstants.CHARACTERS:
                    String text = reader.getText().trim();
                    if (text.isEmpty()) {
                        break;
                    }
                    switch (elementName) {
                        case COUNTRY:
                            voucher.setCountry(text);
                            break;
                        case NUMBER_DAYS:
                            voucher.setNumber_days(text);
                            break;
                        case NUMBER_NIGHTS:
                            voucher.setNumber_nights(text);
                            break;
                        case HOTEL_NAME:
                            voucher.setHotel_name(text);
                            break;
                        case STARS:
                            voucher.setStars(text);
                            break;
                        case BOARD_TYPE:
                            voucher.setBoard_type(text);
                            break;
                        case ROOM_BED_AMOUNT:
                            voucher.setRoom_bed_amount(text);
                            break;
                        case AVAILABLE_AMENITIES:
                            voucher.setAvailable_amenities(text);
                            break;
                        case COST:
                            voucher.setCost(Double.parseDouble(text));
                            break;
                        case INCLUDED_FACTORS_IN_PRICE:
                            voucher.setIncluded_factors_in_price(text);
                            break;
                        case VOUCHER:
                            voucherList.add(voucher);
                            voucher = null;
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    elementName = VoucherTagName.getElementTagName(reader.getLocalName());
                    switch (elementName) {
                        case VOUCHER:
                            voucherList.add(voucher);
                    }
            }
        }
        return voucherList;
    }
}
