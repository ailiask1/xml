package Handler;

import Entity.Voucher;
import Entity.VoucherTagName;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class MenuSaxHandler extends DefaultHandler {
    private List<Voucher> voucherList = new ArrayList<Voucher>();
    private Voucher voucher;
    private StringBuilder text;

    public List<Voucher> getVoucherList() {
        return voucherList;
    }

    public void startDocument() throws SAXException {
        System.out.println("Parsing started.");
    }

    public void endDocument() throws SAXException {
        System.out.println("Parsing ended.");
    }

    public void startElement(String url, String localName, String qName,
                             Attributes attributes) throws SAXException {
        System.out.println("startElement -> " + "url: "
                + url + ", localName: " + localName + ", qName: " + qName);

        text = new StringBuilder();
        if (qName.equals("Voucher")) {
            voucher = new Voucher();
            voucher.setId(((attributes.getValue("id"))));
            voucher.setVoucher_type(attributes.getValue("Voucher_type"));
            voucher.setTransport(attributes.getValue("Transport"));
        }
        if (qName.equals("Hotel_characteristics")) {
            voucher.setHotel_owner(attributes.getValue("Hotel_owner"));
        }
    }

    public void characters(char[] buffer, int start, int length) {
        text.append(buffer, start, length);
    }

    public void endElement(String uri, String localName, String qName)
            throws SAXException {
        VoucherTagName tagName =
                VoucherTagName.valueOf(qName.toUpperCase().replace("-", "_"));
        switch (tagName) {
            case COUNTRY:
                voucher.setCountry(text.toString());
                break;
            case NUMBER_DAYS:
                voucher.setNumber_days(text.toString());
                break;
            case NUMBER_NIGHTS:
                voucher.setNumber_nights(text.toString());
                break;
            case HOTEL_NAME:
                voucher.setHotel_name(text.toString());
                break;
            case STARS:
                voucher.setStars(text.toString());
                break;
            case BOARD_TYPE:
                voucher.setBoard_type(text.toString());
                break;
            case ROOM_BED_AMOUNT:
                voucher.setRoom_bed_amount(text.toString());
                break;
            case AVAILABLE_AMENITIES:
                voucher.setAvailable_amenities(text.toString());
                break;
            case COST:
                voucher.setCost(Double.parseDouble(text.toString()));
                break;
            case INCLUDED_FACTORS_IN_PRICE:
                voucher.setIncluded_factors_in_price(text.toString());
                break;
            case VOUCHER:
                voucherList.add(voucher);
                voucher = null;
                break;
        }
    }

    public void warning(SAXParseException exception) {
        System.err.println("WARNING: line " + exception.getLineNumber() + ": " + exception.getMessage());
    }

    public void error(SAXParseException exception) {
        System.err.println("ERROR: line " + exception.getLineNumber() + ": " + exception.getMessage());
    }

    public void fatalError(SAXParseException exception) throws SAXException {
        System.err.println("FATAL: line " + exception.getLineNumber() + ": "
                + exception.getMessage());
        throw (exception);
    }
}


