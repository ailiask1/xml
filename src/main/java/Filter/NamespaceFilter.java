package Filter;

import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLFilterImpl;

public class NamespaceFilter extends XMLFilterImpl {

    public NamespaceFilter(XMLReader reader){
        super(reader);
    }

    public void startPrefixMapping(String prefix, String url)
            throws SAXException {
        System.out.println("startPrefixMapping in NamespaceFilter -" +
                prefix + ", " + url);
        super.startPrefixMapping(prefix, url);
    }
}

