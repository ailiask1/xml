package Filter;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.XMLFilterImpl;

public class ElementFilter extends XMLFilterImpl {

    public void startElement(String url, String localName, String qName,
                             Attributes attributes) throws SAXException {
        System.out.println("startElement in ElementFilter");
        super.startElement(url, localName, qName, attributes);
    }
}

