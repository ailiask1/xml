package Builder;

import Parser.DomParser;
import Parser.SaxParser;
import Parser.StaxParser;
import org.xml.sax.SAXException;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        SaxParser saxParser = new SaxParser();
        StaxParser staxParser = new StaxParser();
        DomParser domParser = new DomParser();
        System.out.println("SaxParser results in the console, others are in the 'data' package");
        try {
            saxParser.run();
            staxParser.run();
            domParser.run();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
    }
}
